import random
from nltk import NaiveBayesClassifier, classify
from nltk.classify.scikitlearn import SklearnClassifier
from nltk.metrics import ConfusionMatrix
from nltk.metrics.scores import precision, recall
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from src.dataset_loader import *
from src.preprocessing import *


def prepare_dataset(path, pos_neg=False):
    data, sentiment = import_dataset(path, pos_neg)
    tokens = get_all_tokens(data)
    normalized = normalization(tokens)
    model_list_dict = get_dict_model(normalized)
    dataset = get_dataset_with_sentiment(model_list_dict, sentiment)
    return dataset


def counter_sentiment(data):
    c_h, c_a, c_r, c_s = 0, 0, 0, 0
    c_p, c_n = 0, 0
    for (lyrics, sentiment) in data:
        if sentiment == 'happy':
            c_h += 1
            c_p += 1
        elif sentiment == 'angry':
            c_a += 1
            c_n += 1
        elif sentiment == 'relaxed':
            c_r += 1
            c_p += 1
        elif sentiment == 'sad':
            c_s += 1
            c_n += 1
        elif sentiment == 'positive':
            c_p += 1
        elif sentiment == 'negative':
            c_n += 1
        else:
            print('Error', sentiment, lyrics)
    print('#happy =', c_h)
    print('#relaxed =', c_r)
    print('#angry =', c_a)
    print('#sad =', c_s)

    print('#positive =', c_p)
    print('#negaitve =', c_n)


def separate_featureset_label(dataset):
    featuresets = []
    labels = []
    for (f, s) in dataset:
        featuresets.append(f)
        labels.append(s)
    return featuresets, labels


if __name__ == "__main__":
    pos_neg = False
    train_data = prepare_dataset('../datasets/added_songs/train/TrainExpanded3.csv', pos_neg)
    random.shuffle(train_data)

    test_data = prepare_dataset('../datasets/added_songs/test/Test.csv', pos_neg)
    random.shuffle(test_data)

    counter_sentiment(train_data)
    print('TEST')
    counter_sentiment(test_data)

    # # input for NaiveBayesClassifier should be a list of tuples (featureset, label)
    classifier = NaiveBayesClassifier.train(train_data)
    print(classifier.show_most_informative_features(50))

    # classifier = SklearnClassifier(LinearSVC())
    # classifier = SklearnClassifier(MultinomialNB())
    # classifier = SklearnClassifier(LogisticRegression(C=1000, max_iter=1e10))
    # classifier.train(train_data)

    print('Accuracy = ', classify.accuracy(classifier, test_data))

    if pos_neg:
        labels = ['positive', 'negative']
    else:
        labels = ['happy', 'relaxed', 'sad', 'angry']

    refsets = {}
    testsets = {}
    for label in labels:
        refsets[label] = set()
        testsets[label] = set()

    for i, (feats, label) in enumerate(test_data):
        refsets[label].add(i)
        observed = classifier.classify(feats)
        testsets[observed].add(i)

    for label in labels:
        print('label =', label)
        print('Precision:', precision(refsets[label], testsets[label]))
        print('Recall:', recall(refsets[label], testsets[label]))






