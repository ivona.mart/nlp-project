import re
import nltk
from nltk.tag import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer
from nltk import FreqDist
from nltk.corpus import stopwords

def get_all_tokens(dataset):
    """ dataset is a list of song lyrics """
    tokenizer = nltk.RegexpTokenizer(r"\w+")
    tokens = []
    for lyrics in dataset:
        token = tokenizer.tokenize(lyrics)
        tokens.append(token)

    return tokens


def normalization(tokens):
    normalized = []
    stop_words = set(stopwords.words('english'))

    for tokenset in tokens:
        current = []

        for token, tag in pos_tag(tokenset):
            # convert to lowercase
            token = token.lower()

            # ignore stopwords
            if token in stop_words:
                continue

            # remove noise
            token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|' \
                           '(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', token)
            token = re.sub("(@[A-Za-z0-9_]+)", "", token)

            #lemmatize
            if tag.startswith("NN"):        # noun
                pos = 'n'
            elif tag.startswith('VB'):      # verb
                pos = 'v'
            else:
                pos = 'a'                   # adjective

            lemmatizer = WordNetLemmatizer()
            token = lemmatizer.lemmatize(token, pos)

            if len(token) > 1:
                current.append(token.lower())

        normalized.append(current)

    return normalized


def flatten(lst):
    flat = [element for current_list in lst for element in current_list]
    return flat


def get_freq_tuples(tokens):
    freq_dist = FreqDist(tokens)
    return freq_dist


def get_dict_model(tokens):
    model_list_of_dicts = []
    current_dict = {}
    for lyrics in tokens:
        for token in lyrics:
            current_dict[token] = "True"
        model_list_of_dicts.append(current_dict)
        current_dict = {}
    return model_list_of_dicts


def get_dataset(dicts_list, sentiment):
    dataset = []
    for current_dict in dicts_list:
        song = (current_dict, sentiment)
        dataset.append(song)
    return dataset


def get_dataset_with_sentiment(dicts_list, sentiment):
    dataset = []
    for current_id in range(len(dicts_list)):
        song = (dicts_list[current_id], sentiment[current_id])
        dataset.append(song)
    return dataset

