import csv
import sys
csv.field_size_limit(sys.maxsize)


def import_dataset(path, pos_neg=False):
    data = []
    sentiment = []
    with open(path, 'r') as f:
        reader = csv.reader(f)
        cnt = 0
        for row in reader:
            if cnt == 0 and 'Expanded' in path:
                cnt += 1
                continue
            data.append(row[5])
            if pos_neg:
                if row[4] == 'happy' or row[4] == 'relaxed':
                    sentiment.append('positive')
                elif row[4] == 'angry' or row[4] == 'sad':
                    sentiment.append('negative')
                else:
                    print('Error! Wrong sentiment:', row[4])
            else:
                sentiment.append(row[4])
    return data, sentiment
